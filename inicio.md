Proyecto N°2

El proyecto se basa en manipular a nuestro favor el archivo "suministro_alimentos_kcal.csv" el cual nos otorgara 
toda la informacion relacionada a la alimentacion de los paises en un contexto de pandemia, por lo cual, tendremos informacion 
porcentual adicional de: casos activos,descesos,recuperados,entre otros;amen de señalar tambien la cantidad de poblacion de los paises

Dicho proyecto propone ciertas tematicas/hipotesis a comprobar, todo esto apoyado de la recoleccion de datas, las cuales, estaran 
complementadas por sus graficos pertinentes. Estas cuatro tematicas seran señaladas acontinuacion

.....

<< Es común tachar de insaluble el alto consumo de carnes fomentando la obesidad. Por lo tanto, en base a los datos presentados¿Es correcta esta afirmacion? >>
Para sacar nuestras conclusiones optamos por descubrir la siguiente informacion

-Informacion de los paises con mayor consumo de carne (sobre el promedio) con su indice de obesidad	 
-Promedio de obesidad porcentual para los paises con alto consumo de  carne
-Promedio de obesidad porcentual de todos los paises
-Grafica respecto al promedio de carne por pais vs obesidad

Todo lo anterior, nos da a concluir que la carne no es un factor determinante para el alto indice de obesidad
de hecho, el promedio de porcentajes de obesidad de los paises con alto consumo de carne no superaban al porcentaje de obesidad de los paises que mas consumian vegetales y frutas. Sin embargo, no hay que confundir estos numeros con que el alto consumo de carne sea más saludable que lo otro; de hecho, el promedio del porcentaje de obesidad los paises con alto consumo de carne sigue siendo bastante alto						                      

.....

<¿Son los vegetales alimentos por excelencia a la hora de mejorar la salud?. Es más, ¿se podria considerar adecuado este tipo de alimentacion para mantener los indices de obesidad controlados?>
Para esta interesante propuesta recopilamos la siguiente informacion, valiosa para nuestras conclusiones 

-Data de los paises con alto consumo de vegetales y sus caracteristicas (tipos de derivados de vegetales que consumen,indice de obesidad,etc.)
-Promedio de obesidad porcentual para los paises con alto consumo vegetal
-Grafico del data: alto consumo vegetal vs obesidad

De esta hipotesis se pudo concluir que paises como Ghinea, Kuwait tienen un porcentaje considerable de consumo de vegetales, siendo este ultimo el pais que más consume dicho producto.
En base a comparaciones con la Hipotesis 1, se puede evidenciar que los paises con alto consumo de vegetales y frutas no necesariamente son los más saludables, ya que, los promedio de obesidad (porcentual) que tienen, son bastante elevados con respecto a la media. Por ello, afirmar que paises que basan su alimentacion en altas dosis de derivados vegetales y frutas, es completamente falso

......

< Segun la USDA (Departamento de Agricultura de los Estados Unidos) el consumo de verduras y granos corresponden a los alimentos que ocupan el mayor porcentaje de ingesta de dieta diaria saludable. En consecuencia, es pertinente pensar que este tipo de alimentacion aportaran una mejor salud, por ende ¿tendran una mayor resiliencia ante el COVID-19, basandonos en los indices de recuperacion? >	
En esta hipotesis veremos lo siguiente:

-Data de paises sobre el promedio, es decir, una dieta basada en granos y verduras, junto con ciertas caracteristicas puntuales como el indice de recuperados
-Promedios de recuperación de paises con alimentacion saludable (porcentual)
-Promedios de recuperación de todos los paises (porcentual)
-Grafica respecto a los paises con alimentacion saludable vs recuperados

Sabiendo que el promedio de recuperacion de los paises con una alimentacion relativamente saludable son mayores a los de todos los paises,se puede deducir que una dieta balanceada como lo dice la USDA producce un efecto positivo en la recuperacion de gente con COVID-19

......

<Como afectan los desordenes alimenticios en un contexto de pandemia ¿Es la desnutrición quien produce mayor aumento en los decesos por sobre la obesidad?¿Ocurre lo contrario?>

-Tabla correspondiente a paises con alta obesidad (osea, que superen el promedio porcentual de obesidad) y su respectivo indice de fallecidos
-Tabla correspondiente a paises con alta desnutricion(osea, que superen el promedio), ademas de indicar el porcentaje de descesos
-Comparacion de graficos obesidad vs desnutricion

En el analisis de los desordenes alimenticios mencionados en la Hipotesis, al comparar con graficos y numeros, se deduce que quienes tienen una mortalidad mayor son los paises mas obesos. Esto no quiere decir que uno sea mejor que el otro, por el contrario, los dos son igual de malos.
Probablemente las circunstanscias de que lo anterior ocurra sean muchas, entre ellas deducimos que al afectar la obesidad el sistema respitatorio y ser el COVID-19 un virus de naturaleza respiratoria, no es descabellado pensar que los efectos de dicho virus se potencien en los cuerpos de gente con obesidad.  

......

Acontinuacion usted como usuario tendra la libertad de conocer esta informacion como guste seleccionando en el menu otorgado las siguientes opciones: "Hipotesis (1)";para discutir si consumir carne realmente es tan perjucial (en contexto de obesidad) como suelen decir, "Hipotesis 2";para conocer interesantes datos con respecto a los vegetales y su repercucion en la obesidad, "Hipotesis 3"; para hablar acerca estandares de la USDA, especialmente los granos y verduras, y como estos podrian influir en la salud en un contexto de COVID-19, "Hipotesis 4" si lo que quiere es comparar desordenes alimenticios en torno al COVID-19 y su repercucion en la mortalidad; y por ultimo tendra una opcion para salir del programa llamado: "Salir".

Gracias por leer				
