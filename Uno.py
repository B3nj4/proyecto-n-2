import pandas as pd
import matplotlib.pyplot as plt
import opener

#///////////////////////////////PROMEDIO////////////////////////////////////////////////////////
def average_columns(data):
    average_Animal_Products = data["Animal Products"].mean()       
    average_Animal_fats = data["Animal fats"].mean()
    average_Fish_Seafood = data["Fish, Seafood"].mean()
    average_Meat = data["Meat"].mean()
    average_Offals = data["Offals"].mean()

    return average_Animal_Products,average_Animal_fats,average_Fish_Seafood,average_Meat,average_Offals

#//////////////////////////////DATAS CON CONDICIONES////////////////////////////////////////
def data_meat(data,average_Animal_Products,average_Animal_fats,average_Fish_Seafood,average_Meat,average_Offals): 
    other_data = data[(data["Animal Products"] > average_Animal_Products) &
                      (data["Animal fats"] > average_Animal_fats) &
                      (data["Fish, Seafood"] > average_Fish_Seafood) &
                      (data["Meat"] > average_Meat) &
                      (data["Offals"] > average_Offals) &
                      (data["Obesity"].notna()) ].copy()
                       
    return other_data

def fila_meat(other_data):
    fila = other_data.copy()
    fila["Suma"] = other_data[["Animal Products","Animal fats",
                               "Fish, Seafood","Meat","Offals"]].sum(axis=1)
    fila.drop(["Animal Products","Animal fats",
                "Fish, Seafood","Meat","Offals"], axis=1, inplace=True)
    return fila

#//////////////////////////////PROMEDIO_OBESIDAD/////////////////////////////////////////////////
def average_obesity(other_data):
    a_obesity = other_data["Obesity"].mean() 
    return a_obesity

#///////////////////////////////GRAFICAS////////////////////////////////////////////////////////
def graphic_meat(other_data):
    other_data.plot(kind="bar", x="Country",subplots=True,figsize=(12,15)) 
    plt.savefig("graphic_Meat.png")

def g_meats(other_data):
    var = other_data
    xd = var.drop(["Obesity"],axis = 1,inplace = True)
    xd.plot(kind="bar", x="Country")  
    plt.savefig("Second_Meat.png")

#///////////////////////////////MAIN////////////////////////////////////////////////////////////
def main():
    new_data = opener.obtener_columnas(["Country","Obesity","Animal Products","Animal fats","Fish, Seafood","Meat","Offals"])
    total_average = new_data["Obesity"].mean()
    average_Animal_Products,average_Animal_fats,average_Fish_Seafood,average_Meat,average_Offals = average_columns(new_data) 
    other_data= data_meat(new_data,average_Animal_Products,average_Animal_fats,average_Fish_Seafood,average_Meat,average_Offals) 
    a_obesity = average_obesity(other_data)
    fila = fila_meat(other_data)

    
    def hipote_uno():
        menu = [" "] * 8
        menu[0] = "--------------------------------------------------------------------------------------------------------"
        menu[1] = "   Presione(1) para ver data de los paises con alto consumo de carne y sus caracteristicas              "
        menu[2] = "   Presione(2) para ver el promedio de obesidad porcentual para los paises con alto consumo de carne    "
        menu[3] = "   Presione(3) para ver el promedio de obesidad porcentual de todos los paises                          "
        menu[4] = "   Presione(4) para ver grafica respecto al promedio de carne por pais vs obesidad                      "
        menu[5] = "   Presione(5) para ver las conclusiones del tema                                                       "
        menu[6] = "   Presione(6) para volver al menu                                                                      "
        menu[7] = "--------------------------------------------------------------------------------------------------------"
        
        for fila in menu:
            texto = "< "
            texto = texto + fila + " "
            print(texto + " >") 
    
    print("\nHipotesis 1:")
    print("<Es común tachar de insaluble el alto consumo de carnes fomentando la obesidad." 
          " Por lo tanto, en base a los datos presentados ¿Es correcta esta afirmacion?>\n")

    while True:
        
        hipote_uno()
        tecla = input("\nElija las opciones para <hipotesis 1> : ")

        if tecla == "1":
            print(f"\nDATA CON PAISES DE ALTO CONSUMO DE CARNE (superior al promedio): \n{other_data}\n")
            
        elif tecla == "2":
            print(f"\nEL PROMEDIO DE LOS PORCENTAJES DE OBESIDAD PARA LOS PAISES CON ALTO CONSUMO DE CARNE ES: %{a_obesity}\n")
            
        elif tecla == "3":
            print(f"\nPROMEDIO DEL PORCENTAJE DE OBESIDAD DE TODOS LOS PAISES: %{total_average}\n")

        elif tecla == "4":
            print("\nPara entender el grafico debemos saber que:")
            print("En ambos gráficos  se puede observar en el eje X los " 
                  "diversos países que se encuentran sobre el promedio en"
                  " el\nconsumo de carnes y derivados."
                  "En el eje Y en el gráfico superior se visualiza el "
                  "porcentaje de obesidad y el\ngráfico inferior nos muestra"
                  " la suma total de carnes y derivados\n ")
            graphic_meat(fila)

        elif tecla == "5":
            print("\nConcluciones: ")
            print("\nLos datos nos demuestran que, paises que" 
                  "se basan en una dieta"
                  "mayoritariamente de carne como \nlos"
                  "son Jamaica,Denmark,Portugal"
                  "y Francia tienen un promedio"
                  "-entre todos- de %24.09 de indices"
                  "de obesidad siendo los paises \ncon" 
                  "mayor indice de obesidad"
                  "Russian Federation y Latvia y el "
                  "pais con mayor consumo de "
                  "carne (al promediar todos los \ntipos"
                  " de carne por cada pais)"
                  "es Iceland. Como dato extra, el derivado de la carne más"
                  "consumido por estos paises \ncorresponde a los Animal Products"
                  "sin excepcion, el cual superaba con creces las otras variantes"
                  "de la carne y el derivado \nque menos se consume es Offals"
                  "Lo anterior dicho es muy interesante de escuchar, más aún,"
                  "cuando al analizar la <Hipotesis 2>\nla cual habla acerca de"
                  "que tan saluble son los paises con una dieta ligada al"
                  "consumo de vegetales y frutas \n-que por lo general son"
                  "denominados alimentos bastante saludables- tienen %25.6 de"
                  "obesidad, más que los paises de\nimportantes cantidades de carne."
                  "Por lo tanto, seria muy vago señalar que un pais que"
                  "consuma mucha carne, probablemente\nsea un pais con altas"
                  "tasas de obesidad. Esto es muy relativo,ya que, muchos"
                  "factores pueden influir en ello \ncomo la cantidad de deporte"
                  "o actividades al aire libre que se haga, la cultura, "
                  "la disponibilidad de alimentos, etc.\n"
                  "Para terminar, es importante destacar que comer solamente carne"
                  "no es recomendado, ya que, el cuerpo necesita de\ntodos los tipos"
                  "de nutrientes para funcionar y, no porque tenga mejores numeros" 
                  "que los paises con muchisimo\nconsumo de vegetales y frutas, significa"
                  "que sea lo más optimo, es más, al comparar los promedios de obesidad\nde" 
                  "los paises con alto consumo de carne con todos los paises, se superan con"
                  "creces el promedio\ngeneral, deduciendo asi que sigue siendo preocupante"
                  "los numeros de paises carnivoros\n")

        elif tecla == "6":
            break
