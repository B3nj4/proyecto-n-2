import pandas as pd
import opener
import matplotlib.pyplot as plt

#//////////////////////////////////DATA CON CONDICIONES///////////////////////////////////////////////////
def sobre_promedio(dataframe):
    av_obesity = dataframe["Obesity"].mean()
    new_Undernourished = dataframe['Undernourished'].str.replace("<",'').astype(float)
    av_malnourished = new_Undernourished.mean()
    obese = dataframe[(dataframe["Obesity"] > av_obesity) &
                         dataframe["Deaths"]].copy()
    obese.drop(["Undernourished"], axis=1, inplace=True)

    malnourished = dataframe[(new_Undernourished > av_malnourished) &
                       dataframe["Deaths"]].copy()
    malnourished.drop(["Obesity"], axis=1, inplace=True)
    return obese, malnourished

#//////////////////////////////////////////////MAIN/////////////////////////////////////////////////////
def main():
    x = opener.obtener_columnas(["Obesity", "Undernourished", "Deaths"])
    obese, malnourished = sobre_promedio(x)

    dead_obese = obese["Deaths"].mean()
    dead_malnourished = malnourished["Deaths"].mean()

    def hipote_Cuatro():
            menu = [" "] * 7
            menu[0] = "---------------------------------------------------------------------------------"
            menu[1] = "   Presione(1) para ver tabla de obesidad y su respectivo indice de fallecidos   "
            menu[2] = "   Presione(2) para ver tabla de desnutricion e indice de muertos                "
            menu[3] = "   Presione(3) para ver la comparacion de graficos obesidad vs desnutricion      "
            menu[4] = "   Presione(4) para ver las conclusiones del tema                                "
            menu[5] = "   Presione(5) para volver al menu                                               "
            menu[6] = "---------------------------------------------------------------------------------"
            
            for fila in menu:
                texto = "< "
                texto = texto + fila + " "
                print(texto + " >") 

    print("\nHipotesis 4:")
    print("<Como afectan los desordenes alimenticios en un contexto de pandemia" 
          "\n¿Es la desnutrición quien produce mayor aumento en los decesos" 
          "por sobre la obesidad?\n¿Ocurre lo contrario?>\n")

    while True:
        hipote_Cuatro()
        tecla = input("\nElija las opciones para <hipotesis 4> : ")

        if tecla == "1":
            print(f"\nTABLA DE OBESIDAD Y SU RESPECTIVO INDICE DE FALLECIDOS  : \n{obese}\n")
                
        elif tecla == "2":
            print(f"\nTABLA DE DESNUTRICION Y SU RESPECTIVO INDICE DE FALLECIDOS  : \n{malnourished}\n")
            
        elif tecla == "3":
            print("\nPara entender el grafico debemos saber que:")
            print("En la parte superior del grafico se observa en el eje x los indices de obesidad\nde paises sobre la media"
                  "por otra parte, en el eje y se visualiza el porcentaje\nde fallecidos por COVID-19.")
            print("En la zona inferior se aprecia en el eje x los indices de desnutricion,\nescogiendo los paises sobre"
                  "el promedio, mientras que en el eje y se obsersa el\nporcentaje de muertes por COVID-19")
            print("A partir de los graficos se puede puede evidenciar una notable\ndiferencia cuantitativas"
                  "entre estos dos contextos manteniendo la obesidad, una\nmantencion en cifras altas,por otro lado "
                  "la desnutricion se\nmantiene levemente bajo pero con unos picos bastante pronunciados \n(a pesar de no "
                  "superar el pico mas alto de la grafica de obesidad)\n")
            #/////////////////////////////////GRAFICA///////////////////////////////////////////////////   
            fig, ax2 = plt.subplots(nrows=2, ncols=1)
            obese = obese.sort_values("Obesity")
            obese.plot(kind="bar", x="Obesity", y="Deaths",
                                    sort_columns=True, use_index=True,
                                    figsize=(19, 9), ax=ax2[0])

            malnourished= malnourished.sort_values("Undernourished")
            malnourished.plot(kind="bar", x="Undernourished", y="Deaths",
                                sort_columns=True, figsize=(19, 9), ax=ax2[1])
            plt.savefig("cuatro.png")

        elif tecla == "4":
            print("\nConclusiones")
            print("Basandonos en los datos podemos conluir que los paises arriba de la media respecto\n"
                  "a la obesidad, mueren en promedio %f %%\nCaso contrario ocurre con la "
                  "desnutricion, donde mueren en promedio %f %% \nEsto significa que la obesidad induce tasas"
                  "de mortalidad más altas; deduciendo asi,\nque el virus adquiere mayor letalidad"
                  "en personas con obesidad;\nprobablemente esto se deba a los problemas cardiorespiratorios"
                  "\nque padecen los individuos obesos,potenciando la letalidad del virus\nrespiratorio "
                  "dentro del organismo"%(dead_obese,dead_malnourished))
                  
        elif tecla == "5":
            break


