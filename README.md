# Proyecto N°2
Este proyecto consiste en el análisis y manipulación de un archivo ".cvs" titulado "suministro_alimentos_kcal". Dicha documentación, recolecta los porcentajes de alimentos consumidos por cierto país junto con los fenómenos alimenticios que esto conlleva,ademas,se presenta información atingente a la pandemia del "COVID_19" exponiéndose así datos como indices de mortandad, porcentaje de casos recuperados e inclusive porcentajes de casos activos, entre otros. Lo interesante de este trabajo es que aborda ciertas hipótesis de forma objetiva, siendo apoyada por datos irrefutables sumado a gráficos que complementan la veracidad de la afirmación y poder sacar conclusiones adecuadas del tema Entre estas hipótesis están:

1) Es común tachar de insaluble el alto consumo de carnes a pesar de no tener datos para ello. Por lo tanto, en   base a los datos presentados en archivo ya mencionado ¿Es correcta esta afirmación?

2) ¿Son los vegetales alimentos por excelencia a la hora de mejorar la salud?. Es más, ¿se podría considerar adecuado este tipo de alimentación para mantener los indices de obesidad controlados?

3)  De acuerdo a USDA (Departamento de Agricultura de los Estados Unidos) el consumo de verduras y granos corresponden a los alimentos que ocupan el mayor porcentaje de ingesta de dieta diaria saludable. En consecuencia, es pertinente pensar en la relación que esta alimentación pueda tener con los porcentajes de recuperación de los países. ¿Es influyente una buena alimentación basada en granos y vegetales- con los países con altos indices/porcentajes de recuperaciones de "COVID_19"?

4) ¿Cómo afectan los desordenes alimenticios en un contexto de pandemia? ¿Es la desnutrición quien produce mayor aumento en los decesos por sobre la obesidad?¿Ocurre lo contrario?


## Pre-requisitos
Los requisitos previos para poder interactuar correctamente con el programa son: la tenencia de un sistema operativo GNU/linux, tener instalado Python en su versión 3 o superior junto con las librerías "pandas" para la manipulación del archivo y matplotlib.pyplot para la obtención de gráficos pertinentes al caso. Importante destacar la adquisición del archivo "suministro_alimentos_kcal.csv" para poder correrlo,junto con un sofware o editor de texto adecuado para este cometido (por ejemplo: Vim,Visual_Studio,Atom,entre otros)

## Pep8
El código de proyecto se rige por las normas Pep8 siendo evidenciado en:

- En una linea, se utilizan 79 palabras o menos
- Se utiliza de forma correcta la sangria


## Construido con
El programa fue desarrollado a cabalidad con lenguaje de programación Python3

## Librerías utilizada

- pandas
- matplotlib.pyplot

## Autores

- **Benjamin Ignacio Vera Garrido**
- **Michelle Scarlette Valdes Mendez**

*Estudiantes de primer año de Ingeniería Civil en Bioinformática - Universidad de Talca*
