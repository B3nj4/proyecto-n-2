#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import opener
import Uno
import Dos
import Tres
import Cuatro

def menu():
    print("\n")
    menu = [" "] * 8
    menu[0] = "------------------------------------------------"
    menu[1] = "   Presione(1) para Hipotesis 1                 "
    menu[2] = "   Presione(2) para Hipotesis 2                 "
    menu[3] = "   Presione(3) para Hipotesis 3                 " 
    menu[4] = "   Presione(4) para Hipotesis 4                 "
    menu[5] = "   Presione(5) para salir                       "
    menu[6] = "   Presione(6) para ver de que trata el codigo  "
    menu[7] = "------------------------------------------------"
      
    for fila in menu:
        texto = "< "
        texto = texto + fila + " "
        print(texto + " >") 
        
def intera_menu():
    
    while True:
        menu()
        tecla = input("\nPresione(número) para elegir alguna opción mostrada en el menú: ")
        
        if tecla == "1":
            Uno.main()

        elif tecla == "2":
            Dos.main()

        elif tecla == "3":
            Tres.main()

        elif tecla == "4":
           Cuatro.main()

        elif tecla == "5":
            print("\n\n[Programa cerrado]\n       ...\n")
            break

        elif tecla == "6":
            temp = open("inicio.md")
            for linea in temp:
                print(linea)
            

intera_menu()
