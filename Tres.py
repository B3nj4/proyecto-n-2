import pandas as pd
import matplotlib.pyplot as plt
import opener

#///////////////////////////////PROMEDIO////////////////////////////////////////////////////////
def sobre_promedio(dataframe):
    average_Pulses = dataframe["Pulses"].mean()       
    average_Vegetal_Products = dataframe["Vegetal Products"].mean()  
    average_Vegetables = dataframe["Vegetables"].mean()  
    return average_Pulses,average_Vegetal_Products,average_Vegetables

#//////////////////////////////DATAS CON CONDICIONES////////////////////////////////////////////
def data_meat(dataframe,average_Pulses,average_Vegetal_Products,average_Vegetables): 
    other_data = dataframe[(dataframe["Pulses"] > average_Pulses) &
                      (dataframe["Vegetal Products"] > average_Vegetal_Products) &
                      (dataframe["Vegetables"] > average_Vegetables) &
                      (dataframe["Recovered"].notna()) ].copy()
    return other_data      

#//////////////////////////////GRAFICOS/////////////////////////////////////////////////////////
def graphic_USDA(other_data):
    other_data.plot(kind="bar", x="Country", y="Recovered",figsize=(12,12))  
    plt.savefig("Graphic_Tres.png")

#////////////////////////////MAIN//////////////////////////////////////////
def main():    
    vegetales = opener.obtener_columnas(["Country","Pulses","Vegetal Products","Vegetables","Recovered"])
    promedio1,promedio2,promedio3 = sobre_promedio(vegetales)
    mayor_promedio = data_meat(vegetales,promedio1,promedio2,promedio3)
    average_Pulses,average_Vegetal_Products,average_Vegetables = sobre_promedio(vegetales)
    other_data = data_meat(vegetales,average_Pulses,average_Vegetal_Products,average_Vegetables)
    
    a_Recovered= other_data["Recovered"].mean()
    all_recovered = vegetales["Recovered"].mean() 

    def hipote_Tres():
        menu = [" "] * 8
        menu[0] = "------------------------------------------------------------------------------------------------"
        menu[1] = "   Presione(1) para ver data de paises sobre el promedio y ciertas caracteristicas              "
        menu[2] = "   Presione(2) para ver promedios de recuperación de paises con alimentacion saludable          "
        menu[3] = "   Presione(3) para ver promedios de recuperación de todos los paises                           "
        menu[4] = "   Presione(4) para ver grafica respecto a los paises con alimentacion saludable vs recuperados "
        menu[5] = "   Presione(5) para ver las conclusiones del tema                                               "
        menu[6] = "   Presione(6) para volver al menu                                                              "
        menu[7] = "------------------------------------------------------------------------------------------------"
        
        for fila in menu:
            texto = "< "
            texto = texto + fila + " "
            print(texto + " >") 

    print("\nHipotesis 3:")
    print("<Segun la USDA (Departamento de Agricultura de los Estados Unidos) "
          "el consumo de verduras y granos\ncorresponden a los alimentos que "
          "ocupan el mayor porcentaje de ingesta de dieta diaria saludable.\n"
          "En consecuencia, es pertinente pensar que este tipo de alimentacion "
          "aportaranuna mejor\nsalud, por ende ¿tendrán una mayor resiliencia "
          "al COVID-19, basandose en los datos de recuperacion?>\n")

    while True:
        hipote_Tres()
        tecla = input("\nElija lo que quiera hacer en <hipotesis 3> : ")

        if tecla == "1":
            print(f"\nDATA DE PAISES SOBRE EL PROMEDIO: \n{mayor_promedio}\n")
            
        elif tecla == "2":
            print(f"\n-PROMEDIO DE RECUPERACION DE PAISES CON ALIMENTACION SALUDABLE: % {a_Recovered}\n")

        elif tecla == "3":
            print(f"\n-PROMEDIO DE RECUPERACION PARA TODOS LOS PAISES SIN CONDICIONES: % {all_recovered}\n")

        elif tecla == "4":
            print("\nPara entender el grafico debemos saber que:")
            print("En este grafico se puede observar en el eje x los países que"
                  "se encuentran sobre el promedio respecto\nal consumo de vegetales "
                  "y granos, mientras que en el eje y se visualiza el "
                  "porcentaje\nde recuperación de dichos países\n")
            graphic_USDA(other_data)

        elif tecla == "5":
            print("\nSe puede concluir mediante los datos obtenidos,que una alimentación"
                  "saludable a base de granos\ny verduras como indica la USDA tiene"
                  "repercusión en los porcentajes de recuperación de COVID-19\n,ya que, "
                  "al realizar un análisis comparativo entre la recuperación general "
                  "en todos los\npaíses vs la recuperacion en países sobre la media "
                  "en consumo de los alimentos mencionados\nse obtiene que el "
                  "promedio\nde recuperación de estos es mayor que el de los "
                  "países en general\n")
            
        elif tecla == "6":
            break
        