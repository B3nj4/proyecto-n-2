import pandas as pd
import matplotlib.pyplot as plt
import opener

#///////////////////////////////PROMEDIO////////////////////////////////////////////////////////
def average_columns(data):
    average_Cereals_Excluding_Beer = data["Cereals - Excluding Beer"].mean()       
    average_Fruits_Excluding_Wine = data["Fruits - Excluding Wine"].mean()
    average_Vegetal_Products = data["Vegetal Products"].mean()
    average_Vegetable_Oils = data["Vegetable Oils"].mean()
    average_Vegetables = data["Vegetables"].mean()
    return average_Cereals_Excluding_Beer, average_Fruits_Excluding_Wine, average_Vegetal_Products, average_Vegetable_Oils, average_Vegetables

#//////////////////////////////DATAS POR CONDICIONES////////////////////////////////////////
def data_vegetables(data,average_Cereals_Excluding_Beer,average_Fruits_Excluding_Wine,average_Vegetal_Products,average_Vegetable_Oils,average_Vegetables): 
    other_data = data[(data["Cereals - Excluding Beer"] > average_Cereals_Excluding_Beer) &
                      (data["Fruits - Excluding Wine"] > average_Fruits_Excluding_Wine) &
                      (data["Vegetal Products"] > average_Vegetal_Products) &
                      (data["Vegetable Oils"] > average_Vegetable_Oils) &
                      (data["Vegetables"] > average_Vegetables) &
                      (data["Obesity"] != "NA") &
                      (data["Obesity"] != "NaN") ].copy()
    return other_data

def fila_vegetables(other_data):
    fila = other_data.copy()
    fila["Suma"] = fila[["Cereals - Excluding Beer","Fruits - Excluding Wine",
                               "Vegetal Products","Vegetable Oils","Vegetables"]].sum(axis=1)
    fila.drop(["Cereals - Excluding Beer","Fruits - Excluding Wine",
                "Vegetal Products","Vegetable Oils","Vegetables"], axis=1, inplace=True)
    return fila

#//////////////////////////////PROMEDIO_OBESIDAD/////////////////////////////////////////////////
def average_obesity(other_data):
    a_obesity = other_data["Obesity"].mean() 
    return a_obesity

#///////////////////////////////GRAFICAS////////////////////////////////////////////////////////
def graphic_Vegetables(other_data):
    other_data.plot(kind="bar", x="Country",subplots=True,figsize=(12,15)) 
    plt.savefig("Graphic_Vegetables.png")

#///////////////////////////////MAIN////////////////////////////////////////////////////////////
def main():
    new_data = opener.obtener_columnas(["Country","Obesity","Cereals - Excluding Beer","Fruits - Excluding Wine","Vegetal Products","Vegetable Oils","Vegetables"])
    average_Cereals_Excluding_Beer,average_Fruits_Excluding_Wine,average_Vegetal_Products,average_Vegetable_Oils,average_Vegetables = average_columns(new_data) 
    other_data= data_vegetables(new_data,average_Cereals_Excluding_Beer,average_Fruits_Excluding_Wine,average_Vegetal_Products,average_Vegetable_Oils,average_Vegetables) 
    a_obesity = average_obesity(other_data)
    fila = fila_vegetables(other_data)

    def hipote_Dos():
        menu = [" "] * 7
        menu[0] = "--------------------------------------------------------------------------------------------------------"
        menu[1] = "   Presione(1) para ver data de los paises con alto consumo de vegetales y sus caracteristicas          "
        menu[2] = "   Presione(2) para ver el promedio de obesidad porcentual para los paises con alto consumo vegetal     "
        menu[3] = "   Presione(3) para ver un grafico del data: alto consumo vegetal vs obesidad                           "
        menu[4] = "   Presione(4) para ver las conclusiones del tema                                                       "
        menu[5] = "   Presione(5) para volver al menu                                                                      "
        menu[6] = "--------------------------------------------------------------------------------------------------------"
        
        for fila in menu:
            texto = "< "
            texto = texto + fila + " "
            print(texto + " >") 

    print("\nHipotesis 2:")
    print("<¿Son los vegetales alimentos por excelencia a la hora de mejorar la salud?"
          " Es más, ¿se podria considerar adecuado este tipo de alimentacion para mantener los indices de obesidad controlados?>\n")
    
    while True:
        
        hipote_Dos()
        tecla = input("\nElija lo que quiera hacer en <hipotesis 2> : ")

        if tecla == "1":
            print(f"\nDATA CON PAISES DE ALTO CONSUMO VEGETALES Y FRUTAS (superior al promedio): \n{other_data}\n")
            
        elif tecla == "2":
            print(f"\nEL PROMEDIO DE LOS PORCENTAJES DE OBESIDAD PARA LOS PAISES CON ALTO CONSUMO VEGETAL: %{a_obesity}\n")
        
        elif tecla == "3":
            print("\nPara entender el grafico debemos saber que:")
            print("En ambos gráficos  se puede observar en el eje X los" 
                  "diversos países que se encuentran sobre el promedio en el"
                  "\nconsumo de verduras y frutas. En el eje Y del gráfico "
                  "superior se visualiza el porcentaje de obesidad y el\n"
                  "gráfico inferior nos muestra la suma total de \nporcentajes"
                  "de alimentación de verduras y frutas\n")
            graphic_Vegetables(other_data)

        elif tecla == "4":
            print("\nConclusion\n")
            print("Los datos nos demuestran que paises que se basan en una dieta"
                  "mayoritariamente de vegetales\ny frutas como lo son Ghinea, Kuwait y levanon"
                  "tienen un promedio -entre todos- de %25.6 en indices \nde obesidad siendo"
                  "el pais con más obesidad Kuwait."
                  "\nEstos datos obtenidos al compararlos con el consumo de carne"
                  "de la Hipotesis 1 \n-con un %24,09 de indice de obesidad-"
                  "nos da a entender \nque un alto consumo de frutas y vegetales no"
                  "refleja un control en la obesidad, de hecho, los datos arrojaron \nlo contrario "
                  "al tener un promedio de obesidad más alto que el de la carne\n")

        elif tecla == "5":
            break
